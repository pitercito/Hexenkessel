﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossFire : MonoBehaviour {
	public int fireIncrease = 1;
	private int coolDown =30;
	private int counter =0;
	private int steps = 10;
	private int stepsCounter=0;
	public GameObject Boss;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (counter == 30 && stepsCounter <= steps) {
			counter = 0;
			gameObject.transform.localScale += new Vector3 (fireIncrease, fireIncrease, fireIncrease);
			stepsCounter++;
			if (stepsCounter == 3) {
				Boss.SetActive (true);
			}

		}else if(stepsCounter>=steps){
			gameObject.SetActive (false);
		} else {
			counter++;
		}

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lookAtObjectEnemySpawnerScript : MonoBehaviour {
	private float timerCounter;
	public float factor = 0.95f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		circularMoveSpawner ();
	}

	void circularMoveSpawner(){


		float x = Mathf.Sin (timerCounter * 0.152f+factor) * 200f;
		float z = Mathf.Cos (timerCounter * 0.152f+factor) * 200f;
		float y = 0;
		timerCounter += Time.deltaTime;
		transform.position = new Vector3 (x, y, z);
	}
}

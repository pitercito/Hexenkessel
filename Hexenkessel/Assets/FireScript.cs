﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireScript : MonoBehaviour {

	private int currentScore = 0;
	private int oldScore = 0;
	public int fireScoreSteps = 30;
	public float fireIncrease = 20;
	public GameObject Fire;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		currentScore = PlayerPrefs.GetInt ("Score");
		if ((currentScore - oldScore) > fireScoreSteps) {
			oldScore += fireScoreSteps;

			print ("Fire Increased    " + currentScore + "  " + oldScore);
			BigFire ();
		}
	}

	public void BigFire(){
		//if (PlayerPrefs.GetInt ("Score") == 5) {

		gameObject.transform.localScale += new Vector3(0, 0, fireIncrease);
		//}
	}
}

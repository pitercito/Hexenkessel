﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WandScript : MonoBehaviour {

	public int treffer = 0;
	public GameObject bullet;
	public Transform bulletSpawn;
	public GameObject BulletFire;


	public GameObject targetedEnemy;
	//private Component halo;

	private GameObject oldTarget;
	public bool pointerCanStart = false;
	public bool pointsOnTarget = false;

	private List<GameObject> bullets = new List<GameObject> ();
	public int sizeOfBullets = 30;


	// Use this for initialization
	void Start () {
		bullet.SetActive (false);
		poolingBullets();

	}
	
	// Update is called once per frame
	void Update () {
		showTargetedEnemy ();
	}

	public void Shoot(){
	

		if (targetedEnemy) {
			if(targetedEnemy.tag.Equals("Enemy") && targetedEnemy.activeSelf == true) {

				for (int i = 0; i < sizeOfBullets; i++) {
					if (!bullets [i].activeInHierarchy) {
						bullets [i].transform.position = transform.position;
						bullets [i].transform.rotation = transform.rotation;
						bullets [i].GetComponent<BulletScript> ().Target = targetedEnemy;
						bullets [i].SetActive (true);
						break;
					}
				}
			}
		}
		}




	private void poolingBullets(){
		for (int i = 0; i < sizeOfBullets; i++) {
				GameObject bulletInList = (GameObject)Instantiate (bullet);
				bulletInList.SetActive (false);
				bullets.Add (bulletInList);

		}
	}

	public void showTargetedEnemy(){


		if (pointerCanStart) {
			BulletFire.SetActive (true);
		targetedEnemy = GetComponent<VRTK.Examples.VRTK_ControllerPointerEvents_ListenerExample> ().targetObject.transform.root.gameObject;

		Component halo;

			if (targetedEnemy.tag.Equals ("Enemy") && pointsOnTarget) {
				

				if (oldTarget && !oldTarget.name.Equals (targetedEnemy.name)) {
					halo = oldTarget.GetComponent ("Halo");
					halo.GetType ().GetProperty ("enabled").SetValue (halo, false, null);
				}

				halo = targetedEnemy.GetComponent ("Halo");
				halo.GetType ().GetProperty ("enabled").SetValue (halo, true, null);
				oldTarget = targetedEnemy;
			}else if(oldTarget && !pointsOnTarget){
				halo = oldTarget.GetComponent ("Halo");
				halo.GetType ().GetProperty ("enabled").SetValue (halo, false, null);
				BulletFire.SetActive (false);
			}

		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour {

	[Tooltip("The position of the FPS text within the headset view.")]
	public Vector3 position = Vector3.zero;
	public float timer;
	private Text TextTimer;

	// Use this for initialization
	void Start () {
		transform.parent.GetComponent<Canvas>().planeDistance = 0.5f;
		TextTimer = GetComponent<Text>();
		TextTimer.transform.localPosition = position;
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		TextTimer.text = Mathf.Round(timer).ToString();
		if (timer <= 0) {
			Application.LoadLevel ("034_Controls_InteractingWithUnityUI");
		}
	}
}

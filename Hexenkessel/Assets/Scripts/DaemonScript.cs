﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DaemonScript : MonoBehaviour {

	public Transform Player;
	public int movementCD = 20;
	public int counterCD = 0;
	public int speed = 10;




	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt(Player);

		upAndDownMovement ();
	}

	void OnEnable(){
		Component halo = gameObject.GetComponent ("Halo");
		halo.GetType ().GetProperty ("enabled").SetValue (halo, false, null);
	}

	void upAndDownMovement(){
		if (counterCD <= movementCD) {
			transform.Translate(Vector3.up * Time.deltaTime * speed);
			counterCD++;
		} else if (counterCD <= 2 * movementCD) {
			transform.Translate (Vector3.down * Time.deltaTime * speed);
			counterCD++;
		} else {
			counterCD = 0;
		}
	}

}

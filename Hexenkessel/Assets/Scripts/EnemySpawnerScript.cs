﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerScript : MonoBehaviour {
	private List<GameObject> rings = new List<GameObject>();
	private List<GameObject> devils = new List<GameObject>();
	private List<GameObject> goblins = new List<GameObject>();
	public GameObject ring;
	public GameObject devil;
	public GameObject goblin;

	private int poolSize = 42;
	private int poolAmount = 14;
	public float fireTime = 5.6f;
	private float RandomNumber;

	private int grpCounterDevil = 0;
	private int grpCounterGoblin = 0;

	public float fireRingsFactor = 1.0f;
	public float fireEnemiesFactor = 1.5f;

	public Transform lookAtObject;



	public float timerCounter;
	// Use this for initialization
	void Start () {
		poolEnemies ();

		InvokeRepeating("FireRings", fireTime * fireRingsFactor, fireTime * fireRingsFactor);
		InvokeRepeating("FireEnemies", fireTime * fireEnemiesFactor, fireTime * fireEnemiesFactor);
	}

	// Update is called once per frame
	void Update () {
		transform.LookAt (lookAtObject);
		circularMoveSpawner ();
	}

	void poolEnemies(){
		for (int i = 0; i < poolSize; i++) {
			if (i >= 0 && i <= 13) {
				GameObject ringInList = (GameObject)Instantiate (ring);

				ringInList.SetActive (false);
				rings.Add (ringInList);
			}
			else if (i >= 14 && i <= 27) {
				GameObject devilInList = (GameObject)Instantiate (devil);
				devilInList.name = "devil" + i;
				devilInList.SetActive (false);
				devils.Add (devilInList);
			}
			else if (i >= 28 && i <= 41) {
				GameObject goblinInList = (GameObject)Instantiate (goblin);
				goblinInList.name = "goblin" + i;
				goblinInList.SetActive (false);
				goblins.Add (goblinInList);
			}
		}
	}

	void FireRings()
	{	
		Vector3 rndPosition = new Vector3(transform.position.x, Mathf.Clamp(Random.Range(-20.2f, 20.2f), -20f, 20f), transform.position.z);
		//Debug.Log(rndNumber);


		for (int i = 0; i < poolAmount; i++)
		{
			if (!rings[i].activeInHierarchy)
			{
				rings [i].transform.position = rndPosition;
				rings[i].transform.rotation = transform.rotation;
				rings[i].SetActive(true);
				break;
			}
		}
	}

	void FireEnemies(){
		int chooseEnemy = Random.Range (0, 2);
		int sizeofEnemyGroup = Random.Range (0,3);

		switch (chooseEnemy) {
		case 0:
			for (int i = 0; i < poolAmount; i++) {
				if (!devils [i].activeInHierarchy) {

					//Give Daemon new Name for Shoot
					//if(devils[i].transform.childCount>0){
					//	devils [i].transform.GetChild (0).transform.name = "Devil" + i;
					//}


					if (grpCounterDevil <= sizeofEnemyGroup) {
						int rndY = 0;
						float rndXZ = 0; 

						switch (grpCounterDevil) {
						case 0:
							rndY = 0;
							rndXZ = 1.2f;
							break;
						case 1:
							rndY = -16;
							rndXZ = 1.3f;
							break;
						case 2:	
							rndY = -16;
							rndXZ = 1.1f;
							break;
						}

						devils [i].transform.position = new Vector3(transform.position.x * rndXZ, rndY, transform.position.z * rndXZ);
						devils [i].transform.rotation = transform.rotation;
						devils [i].SetActive (true);
						grpCounterDevil += 1;
					} else {
						grpCounterDevil = 0;
						break;
					}
				}

			}
			break;
		case 1:


			for (int i = 0; i < poolAmount; i++) {
				if (!goblins [i].activeInHierarchy) {

					if (grpCounterGoblin <= sizeofEnemyGroup) {
						int rndY = 0;
						float rndXZ = 0; 

						switch (grpCounterGoblin) {
						case 0:
							rndY = 0;
							rndXZ = 0.8f;
							break;
						case 1:
							rndY = -16;
							rndXZ = 0.7f;
							break;
						case 2:	
							rndY = -16;
							rndXZ = 0.9f;
							break;
						}

						//Give Goblin new Name for Shoot
						if(goblins[i].transform.childCount>0){
						goblins [i].transform.GetChild (0).transform.name = "Goblin" + i;
						}
							
						goblins[i].transform.position = new Vector3(transform.position.x * rndXZ, rndY, transform.position.z * rndXZ);
						goblins [i].transform.rotation = transform.rotation;
						goblins [i].SetActive (true);

						grpCounterGoblin += 1;
					} else {
						grpCounterGoblin = 0;
						break;
					}
				}

			}
			break;
		}
	}

	void circularMoveSpawner(){


			float x = Mathf.Sin (timerCounter * 0.152f+1) * 200f;
			float z = Mathf.Cos (timerCounter * 0.152f+1) * 200f;
			float y = 0;
			timerCounter += Time.deltaTime;
			transform.position = new Vector3 (x, y, z);
	}


}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularMovement : MonoBehaviour {
	float timecounter;



	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		circularMove ();
	}
	void circularMove(){
		
		float x = Mathf.Sin (timecounter * 0.152f) * 200f;
		float z = Mathf.Cos (timecounter * 0.152f) * 200f;
		float y = 0;
		timecounter += Time.deltaTime;
		transform.position = new Vector3 (x, transform.position.y, z);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timeToBeAlive_EnemiesScript : MonoBehaviour {
	public float dieTime = 2f;
	// Use this for initialization
	void Start () {
	}
	// Update is called once per frame
	void Update () {
	}
	void OnEnable(){
		Invoke ("DieAfterTime", dieTime);
	}
	void DieAfterTime(){
		gameObject.SetActive (false);
	}
	void OnDisable(){
		CancelInvoke ();
	}
}

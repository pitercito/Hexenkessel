﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		FlyRestrictions ();
	}

	public void FlyRestrictions(){
		transform.position = new Vector3 (0,Mathf.Clamp(transform.position.y,-110.3F,129.3F),0);
	}
}

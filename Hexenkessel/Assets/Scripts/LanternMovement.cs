﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanternMovement : MonoBehaviour {

	public int movementCD = 20;
	public int counterCD = 0;
	public float speed = 10;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		upAndDownMovement ();
	}

	void upAndDownMovement(){
		if (counterCD <= movementCD) {
			transform.Translate(Vector3.forward * Time.deltaTime * speed);
			counterCD++;
		} else if (counterCD <= 2 * movementCD) {
			transform.Translate (Vector3.back * Time.deltaTime * speed);
			counterCD++;
		} else {
			counterCD = 0;
		}
	}
}

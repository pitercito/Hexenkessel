﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemonLordShootingScript : MonoBehaviour {
	private List<GameObject> hellFires = new List<GameObject>();
	private int sizeOfHellFires = 15;
	public GameObject hellFire;

	public int coolDown = 60;
	public int counter = 0;
	// Use this for initialization
	void Start () {
		poolingHellFire ();
	}
	
	// Update is called once per frame
	void Update () {
		if (counter == coolDown) {
			Shoot ();
			counter = 0;
		} else {
			counter++;
		}
	}

	public void Shoot(){

		for (int i = 0; i < sizeOfHellFires; i++) {
			if (!hellFires [i].activeInHierarchy) {
				hellFires [i].transform.position = transform.position;
				hellFires[i].transform.rotation = transform.rotation;
				hellFires[i].SetActive (true);
				break;
			}
		}
	}



	private void poolingHellFire(){
		for (int i = 0; i < sizeOfHellFires; i++) {
			GameObject hellFireInList = (GameObject)Instantiate (hellFire);
			hellFireInList.SetActive (false);
			hellFires.Add (hellFireInList);

		}
	}
    // use this tutorial
  //  https://forum.unity3d.com/threads/unity-turret-tutorial.341866/
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DaemonLordScript : MonoBehaviour
{

    public Transform player;
    public float smoothing = 10.0f;
    Quaternion lookAtRotation;
    public float horizontailOffset = 30.0f; // angle at which it looks in, change to look infront or behind player
    public float verticalOffset = -50.0f; // angle at which it looks in, change to look above or below player
    Vector3 tmp;
	public GameObject entranceFire;
	public Text text;
	string test= "http://localhost/UnityTest/UnityForm.php";

    void Start()
    {
		PlayerPrefs.SetString ("name","Peter");
		PlayerPrefs.SetInt ("Score",3);
		CreateHighscore (PlayerPrefs.GetString("name"), PlayerPrefs.GetInt("Score"));
		
    }

    void Update()
    {
		
//    

		if (transform.position.y >= 110) {
			transform.Translate (Vector3.down/3);
		} else {
			entranceFire.SetActive (false);
			lookAtRotation = Quaternion.LookRotation(player.transform.position - transform.position);
			
			        if (transform.rotation != lookAtRotation)
			        {
			            tmp = lookAtRotation.eulerAngles;
			            tmp.y += horizontailOffset;                           // look slightly infront of the player
			            tmp.x = (tmp.z * -1) - verticalOffset;          // uses the x value to aim vertically and iff offset so it aims at the center of the target
			            tmp.z = 0;                              // Should never rotate
			
			            Quaternion newRot = Quaternion.Euler(tmp);
			
			            transform.rotation = Quaternion.RotateTowards(transform.rotation, newRot, Time.deltaTime * smoothing * 2);
			        }
			StartCoroutine (Test ());
		}
    }
	IEnumerator Test(){
			yield return new WaitForSeconds (4);
			Application.LoadLevel ("034_Controls_InteractingWithUnityUI");
			//Application.LoadLevel ("034_Controls_InteractingWithUnityUI");
	}

	public void CreateHighscore(string name,int score){
		WWWForm form = new WWWForm ();
		form.AddField ("usernamePost", name);
		form.AddField ("scorePost", score);

		WWW www = new WWW (test, form);
	}


}

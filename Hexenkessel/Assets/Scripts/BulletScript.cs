﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

	//public Transform targetOfBullet;
	public GameObject Target;
	private int score;
	private GameObject lookAtTarget;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		//GameObject Targets = GameObject.Find(PlayerPrefs.GetString("NameOfObject"));
		if (lookAtTarget.activeInHierarchy) {

			transform.LookAt (new Vector3 (Target.transform.position.x, Target.transform.position.y, Target.transform.position.z));
		}
			transform.Translate (Vector3.forward * Time.deltaTime * 100);

	}

	void OnCollisionEnter(Collision col){
		
		if (col.transform.root.gameObject.tag.Equals("Enemy")) {
			

			//Fuer Highscore
			score = PlayerPrefs.GetInt ("Score");
			PlayerPrefs.SetInt ("Score", score + 2);
			col.transform.root.gameObject.SetActive (false);
			gameObject.SetActive (false);
		}

	}
	void OnEnable(){
		if (Target) {
			lookAtTarget = GameObject.Find(Target.name);
		}
	}

}

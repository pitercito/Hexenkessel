﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameOverText : MonoBehaviour {
	public float timer;
	public GameObject GameOver;
	public GameObject Player;
	public GameObject Fire;
	public Text text;

	string test= "http://localhost/UnityTest/UnityForm.php";


	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt ("Score",3);
		CreateHighscore (PlayerPrefs.GetString("name"), PlayerPrefs.GetInt("Score"));
		print (PlayerPrefs.GetInt ("Score"));
		GameOver.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		if (timer <= 0) {
			//GameOver.SetActive (true);
			Player.GetComponent<Rigidbody> ().useGravity=true;
			StartCoroutine (Test ());

		}
	}

	IEnumerator Test(){
		
		Fire.SetActive (true);

		yield return new WaitForSeconds (4);
		//Application.LoadLevel ("034_Controls_InteractingWithUnityUI");
	}

	public void CreateHighscore(string name,int score){
		WWWForm form = new WWWForm ();
		form.AddField ("usernamePost", name);
		form.AddField ("scorePost", score);

		WWW www = new WWW (test, form);
	}
}

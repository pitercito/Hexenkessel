﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

	public class BesenScript : MonoBehaviour {

	float fixPointDifference;
	float fixPoint;
	public GameObject controllerRight;
	public GameObject player;
	public bool fixPointSelected = false;
	public int speed = 32;
	float acceleration;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		MoveOnY ();
		player.transform.position = new Vector3 (player.transform.position.x,Mathf.Clamp(player.transform.position.y,-30.3F,20.3f),player.transform.position.z);
	}

	public void Test(){
		if (!fixPointSelected) {
			fixPoint = controllerRight.transform.localPosition.y;

			fixPointSelected = true;
		}
	}

	public void MoveOnY(){
		

		acceleration = controllerRight.transform.localPosition.y - fixPoint;



		if (fixPointSelected) {
			if (controllerRight.transform.localPosition.y > fixPoint) {
				player.transform.Translate (new Vector3(0, speed * acceleration * Time.deltaTime, 0));
			}else if(controllerRight.transform.localPosition.y < fixPoint){
				player.transform.Translate (new Vector3(0, speed * acceleration * Time.deltaTime, 0));
			}
		}

	}
}


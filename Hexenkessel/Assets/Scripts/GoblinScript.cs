﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoblinScript : MonoBehaviour {
	
	public Transform Player;
	public int movementCD = 20;
	public int counterCD = 0;
	public int speed = 10;
	private int saveSpeed;

	// Use this for initialization
	void Start () {
		saveSpeed = speed;
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt(Player);
		RotateLeftAndRight ();
	}

	void OnEnable(){
		Component halo = gameObject.GetComponent ("Halo");
		halo.GetType ().GetProperty ("enabled").SetValue (halo, false, null);
	}

	void RotateLeftAndRight(){
		if (counterCD <= movementCD) {
			transform.Translate (Vector3.up * Time.deltaTime * speed);
			counterCD++;
			speed -= speed/movementCD;
		} else if (counterCD <= 2 * movementCD) {
			transform.Translate (Vector3.down * Time.deltaTime * speed);
			counterCD++;

		} else {
			counterCD = 0;
			speed = saveSpeed;
		}

	}
}

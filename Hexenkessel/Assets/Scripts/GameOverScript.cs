﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour {

	[Tooltip("The position of the FPS text within the headset view.")]
	public Vector3 position = Vector3.zero;
	private Text GameOverText;
	public GUIText text;

	// Use this for initialization
	void Start () {
		
		transform.parent.GetComponent<Canvas>().planeDistance = 0.5f;
		GameOverText = GetComponent<Text>();
		GameOverText.transform.localPosition = position;
	}

	// Update is called once per frame
	void Update () {
	}
}

﻿namespace VRTK.Examples
{
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class UI_Interactions : MonoBehaviour
    {

		public Text ScoreText;
		public GameObject RightToolTip;
		public GameObject LeftToolTip;
		public GameObject KeyBoard;
		public GameObject StartGameButton;

		void Start(){
			ScoreText.text ="Hier kommt der beste Score mit den Namen";
			KeyBoard.SetActive (false);
		}


        public void Button_Pink()
        {
			KeyBoard.SetActive (true);
			StartGameButton.SetActive (false);


        }

       
    }
}
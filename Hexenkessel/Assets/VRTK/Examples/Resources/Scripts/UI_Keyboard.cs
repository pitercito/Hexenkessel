﻿namespace VRTK.Examples
{
    using UnityEngine;
    using UnityEngine.UI;

    public class UI_Keyboard : MonoBehaviour
    {
        private InputField input;
		public GameObject KeyBoard;
		public string name;
		public Text textName;



        public void ClickKey(string character)
        {
            input.text += character;
        }

        public void Backspace()
        {
            if (input.text.Length > 0)
            {
                input.text = input.text.Substring(0, input.text.Length - 1);
            }
        }

        public void Enter()
        {

			print (textName.text);
			PlayerPrefs.SetString ("name",textName.text);

            Debug.Log("You've typed [" + input.text + "]");
            input.text = "";
			KeyBoard.SetActive (false);
			Application.LoadLevel ("MainScene");



        }

        private void Start()
        {
            input = GetComponentInChildren<InputField>();
        }
    }
}